<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique()->default('');
            $table->string('email')->unique()->default('');
            $table->string('password');
            $table->smallInteger('login_count')->default(0);
            $table->char('last_ip', 20)->default('');
            $table->timestamp('last_at')->nullable();
            $table->tinyInteger('is_use')->default(1)->comment('是否允许登陆1是0否');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
