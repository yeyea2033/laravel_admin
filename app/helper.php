<?php
/**
 * 自定义辅助函数
 * User: Administrator
 * Date: 2018/4/24
 * Time: 16:54
 */
define('SGLOGS_PATH', str_replace('\\', '/', dirname(__DIR__)) . '/' . 'public/logs/');
if (!function_exists('mylog')) {
    /**
     * 写日志
     * @param $data
     * @param string $flag
     * @param bool $is
     * @param string $title
     * @return bool
     */
    function mylog($data, $flag = 'None', $is = false, $title = '断点日志')
    {
        return \App\Plugin\SgLogs::write($data, $flag, $is, 'debug', $title);
    }
}
/**
 * 将BIT转换成易看的单位
 * @param $size
 * @param int $digits
 * @return string
 */
function size2mb($size, $digits = 2)
{ //digits，要保留几位小数
    $unit = array('', 'K', 'M', 'G', 'T', 'P');//单位数组，是必须1024进制依次的哦。
    $base = 1024;//对数的基数
    $i    = floor(log($size, $base));//字节数对1024取对数，值向下取整。
    $n    = count($unit);
    if ($i >= $n) {
        $i = $n - 1;
    } elseif ($i <= 0) {
        $i = 0;
    }
    return round($size / pow($base, $i), $digits) . ' ' . $unit[$i] . 'B';
}

/**
 * 下载文件,隐藏下载路径
 * @param $file
 * @return bool
 */
function sg_download($file)
{
    if (!is_file($file))
        return false;
    set_time_limit(0);
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_end_clean();
    flush();
    readfile($file);
    exit;
}
if (!function_exists('sgConfig')) {
    /**
     * 获取系统配置
     * @param $name
     * @param string $default
     * @return mixed
     */
    function sgConfig($name, $default = '')
    {
        return \App\Models\Configs::getValue($name, $default);
    }
}