<div class="layui-row">
    <div class="layui-col-md4">
        <label class="layui-form-label">开发者ID</label>
        <div class="layui-input-block">
            <input type="text" name="app_id"  lay-verType="tips"
                   placeholder="请输入开发者ID(AppID)" value="{{$item['app_id'] or ''}}"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
</div>
<div class="layui-row mt5">
    <div class="layui-col-md4">
        <label class="layui-form-label">开发者密码</label>
        <div class="layui-input-block">
            <input type="text" name="app_secret"  lay-verType="tips"
                   placeholder="请输入开发者密码(AppSecret)" value="{{$item['app_secret'] or ''}}"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
</div>
<div class="layui-row mt5">
    <div class="layui-col-md4">
        <label class="layui-form-label">服务器地址</label>
        <div class="layui-input-block">
            <input type="text" value="" placeholder="适合认证订阅或服务号"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-col-md6">
        <div class="o5 mt2 ml2">适合认证订阅或服务号</div>
    </div>
</div>
<div class="layui-row mt5">
    <div class="layui-col-md4">
        <label class="layui-form-label">服务器地址</label>
        <div class="layui-input-block">
            <input type="text" value="" placeholder="适合个人订阅未认证号"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-col-md6">
        <div class="o5 mt2 ml2">适合个人订阅未认证号</div>
    </div>
</div>
<div class="layui-row mt5">
    <div class="layui-col-md4">
        <label class="layui-form-label">令牌</label>
        <div class="layui-input-block">
            <input type="text" name="wx_token"  lay-verType="tips" placeholder="令牌(Token)"
                   autocomplete="off" class="layui-input" value="{{$item['wx_token'] or ''}}">
        </div>
    </div>
</div>
<div class="layui-row mt5">
    <div class="layui-col-md4">
        <label class="layui-form-label">消息加解密密钥</label>
        <div class="layui-input-block">
            <input type="text" name="wx_aeskey"  lay-verType="tips"
                   placeholder="消息加解密密钥(EncodingAESKey)" value="{{$item['wx_aeskey'] or ''}}"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
</div>
